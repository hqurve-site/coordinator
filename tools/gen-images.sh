#!/usr/bin/env bash

set -e

export time=$(date +%s)
export maxage=600 # time in seconds

myconvert () {
    file=$1
    outfile="${file%%.svg}.png"
    outpdf="${file%%.svg}.pdf"
    lastedit=$(stat -c "%Z" "$file")

    if [[ $(($lastedit + $maxage)) -ge $time || ! -f "$outfile" ]]; then
        echo "$file -> $outfile"
        inkscape "$file" --export-dpi=240 --export-area-page -o "$outfile"
    fi
    if [[ $(($lastedit + $maxage)) -ge $time || ! -f "$outpdf" ]]; then
        echo "$file -> $outpdf"
        inkscape "$file" --export-area-page -o "$outpdf"
    fi
}

export -f myconvert
find content -wholename '*/images/*.svg' -exec bash -c 'myconvert "{}"' \;
