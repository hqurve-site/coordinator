// @ts-check
// this script needs to be run in the output directory
// This script only exists since latexmk does not run builds in parallel

const os = require('os');
const fs = require('fs');
const path = require('path');
const async = require('async');
const { exec } = require('child_process');

const LATEX_DIRECTORY=path.join(process.cwd(), "__convert-to-latex");
const PAGE_LATEX_DIRECTORY = path.join(LATEX_DIRECTORY, "page-latex");
const COMPONENT_LATEX_DIRECTORY = path.join(LATEX_DIRECTORY, "latex");

// number of calls to run in parallel
const N = os.cpus().length * 2;


try {
  main()
}catch(error) {
  console.log(error)
}
async function main() {
  // first link images directory if not already done
  if (!fs.existsSync(path.join(PAGE_LATEX_DIRECTORY, "images"))) {
    fs.symlinkSync(path.join(COMPONENT_LATEX_DIRECTORY, "images"), path.join(PAGE_LATEX_DIRECTORY, "images"));
  }

  // next, get a list of jobs (tex files)
  let list =
    [
      fs.readdirSync(COMPONENT_LATEX_DIRECTORY)
        .filter(name => name.endsWith(".tex"))
        .map(name => path.join(COMPONENT_LATEX_DIRECTORY, name))
      ,
      fs.readdirSync(PAGE_LATEX_DIRECTORY)
        .filter(name => name.endsWith(".tex"))
        .map(name => path.join(PAGE_LATEX_DIRECTORY, name))
    ].flat();

  // run all in parallel
  await async.parallelLimit(list.map(name => callback => {
    // check for fatal error
    // For some reason latexmk does not like to rerun in this case.
    // So, we just manually detect it and then remove aux file to force rebuild
    if (fs.existsSync(name.replace(/\.tex$/, '.log'))) {
      let log_file = fs.readFileSync(name.replace(/\.tex$/, '.log'));
      if (log_file.includes("!  ==> Fatal error occurred, no output PDF file produced!")) {
        fs.rmSync(name.replace(/\.tex$/, '.aux'))
      }
    }

    // -f to force to rerun if there are any errors
    let command = `latexmk -pdflatex=lualatex -pdf -f -interaction=nonstopmode -silent "${path.basename(name)}"`;
    console.log(`running "${command}"`);
    exec(command, { cwd: path.dirname(name) },
      (error, _stdout, _stderr) => {
        if (!error)  console.log(`done ${name}`);
        // @ts-ignore
        callback(error)
      }
    )
  }), N);

  // now map the files to their correct locations
  let lines = fs.readFileSync(path.join(LATEX_DIRECTORY, "page-latex-map.txt"), {encoding: "utf8"}).split("\n");
  // skip first line
  for (let i=1; i <lines.length ; i+= 2) {
    fs.copyFileSync(path.join(PAGE_LATEX_DIRECTORY, lines[i]), lines[i+1]);
  }

}
