#!/usr/bin/env bash

exec_loc=$(dirname "$(realpath "$0")")

infile=$(realpath "$1")
outpath=$(realpath "$2")

mkdir -p "$outpath/modules/ROOT/pages"

dir=$(dirname "$infile")
pushd "$dir" > /dev/null

converted=$(pandoc "$infile" -t "asciidoc")

cleaned=$(echo -n "$converted" | "$exec_loc/clean.js")

# echo "$cleaned"
echo "$cleaned" | "$exec_loc/split.js" "$outpath"
