#!/usr/bin/env node

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

const lines = [];
rl.on('line', function(line){
    lines.push(line);
});

rl.on('close', function(){
    const full = lines.join("\n");

    const cleaned = full
        .replace(/latexmath/g, "stem")
        .replace(/stem:\[\$(.*?)\$\]/g, (match, p) => 
            `stem:[${p.replace(/\\?\]/g, "\\]")}]`
        )
        .replace(/\[stem\]\s*\+{4}\s*\\\[(.*?)\\\]\s*\+{4}/gs, "[stem]\n++++\n$1\n++++")
        .replace(/_Proof\._\s*(2em(\s*\.)?)?(.*?\u{25FB})/gsu, ".Proof\n[%collapsible]\n====\n$3\n====")
        .replace(/^=(=*) /gm, "$1 ")

    console.log(cleaned);
});
