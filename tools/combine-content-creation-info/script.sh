#!/usr/bin/env bash

set -e # exit on error

# uses git-filter-repo
git-filter-repo --help > /dev/null # check if it is present (exits if not present since exit on error)

tmpdir=$(mktemp -d)
echo $tmpdir
trap 'rm -rf $tmpdir' EXIT # autodelete on exit

pushd $tmpdir

# add main
git clone git@gitlab.com:hqurve-site/content-old/main.git main
pushd main
git filter-repo --to-subdirectory-filter main
git filter-repo --message-callback "return b'main: ' + message"
popd

# add food
git clone git@gitlab.com:hqurve-site/content-old/food.git food
pushd food
git filter-repo --to-subdirectory-filter food
git filter-repo --message-callback "return b'food: ' + message"
popd

# add fun
git clone git@gitlab.com:hqurve-site/content-old/fun.git fun
pushd fun
git filter-repo --to-subdirectory-filter fun
git filter-repo --message-callback "return b'fun: ' + message"
popd

# add notes
git clone git@gitlab.com:hqurve-site/content-old/notes.git notes
pushd notes
# check list of commits: git log --format="%at %H" --reverse | cb
# check fix out of order commits
git filter-repo --commit-callback '
  if commit.original_id == b"d76b2e6f669f9655dfc460555cc451a8dace965d":
    commit.author_date = b"1631482600 -0400" # init at start of commits
  elif commit.original_id == b"6b2afdcfac137af08318a15963bec638a5e3f1e9":
    commit.author_date = b"1634301300 -0400" # I dont know why this was out of order
  '
git filter-repo --subdirectory-filter components
popd

# make new output
mkdir new
pushd new
git init
git remote add old_main ../main
git remote add old_fun ../fun
git remote add old_food ../food
git remote add old_notes ../notes
git fetch --all
commits="old_main/master old_fun/master old_food/master old_notes/master"

# ensure all commits are in order
for githash in $(echo $commits); do
  echo "checking $githash"
  # https://stackoverflow.com/questions/53687124/bash-check-if-file-is-sorted-with-sort-c-file
  # sort based on first field (numerically)
  git log $githash --format="%at %H" --reverse | sort -k1,1 -n -C
  echo "$githash sorted"
done

# based on https://stackoverflow.com/questions/7575367/combining-unrelated-git-repositories-retaining-history-branches
# git log [unixtimestamp]
# cut to get second field. Space separated due to the format specified in git log command
echo $commits | xargs -n 1 git log --format="%at %H" | sort -k1,1 -n | cut -f2 -d " " | xargs -n 1 git cherry-pick

# set commit date to author date
git filter-repo --force --commit-callback 'commit.committer_date = commit.author_date'
popd

popd # exit temp directory

git clone $tmpdir/new --single-branch content
