// @ts-check

/** @type any */
const simpleGit = require('simple-git');

// manually inspect the log to determine the last commit prior to merge
const roots = {
  // "notes": "math6192: add more linear stability analysis", // do nothing
  "main": "fix link to PG probability",
  "food": "bread: fix table",
  "fun": "WIP: add math modelling workshop",
};

main().then(() => process.exit())
async function main() {
  process.chdir("../../") // change root for filter-branch
  const git = simpleGit({
    baseDir: process.cwd(),
  });

  /** @type {{all: Array<{message:string, hash: string}>}}*/
  let log = await git.log();

  // for each root find the commit
  /** @type {Object<string,string>} */
  let root_commits = {};
  for (let l of log.all) {
    let matching_root = Object.entries(roots).find(([r, n]) => n === l.message);
    if (matching_root !== undefined) {
      root_commits[matching_root[0]] = l.hash;
    }
  }
  
  console.log(root_commits);

  // determine the commits to rename and their new names
  /** @type {[[string,string]]} */
  let commit_adjusted_names = [];
  for (let [root, hash] of Object.entries(root_commits)) {
    // get log
    let branch_log = await git.log([hash]);
    for (let l2 of branch_log.all) {
      commit_adjusted_names.push([l2.hash, `${root}: ${l2.message}`]);
    }
  }

  // get the start of each chain too
  let chain_starts = {};
  /** @type {Object<string,string>} */
  for (let [root, hash] of Object.entries(root_commits)) {
    let branch_log = await git.log([hash]);
    chain_starts[root] = branch_log.all[branch_log.all.length - 1].hash;
  }

  console.log(chain_starts);

  // create script to edit commit
  // Inspired by https://stackoverflow.com/questions/33866185/rename-multiple-names-and-emails-in-a-single-pass-of-git-filter-branch

  // before adjusting the messages, create a temporary branch since adjusting the messages
  // will cause commits to change
  for (let root in root_commits) {
    await git.raw(['tag', `temp-branch_start__${root}`, chain_starts[root]]);
    await git.raw(['tag', `temp-branch_end__${root}`, root_commits[root]]);
  }

  // adjust messages
  for (let root in root_commits) {
    await git.raw(['filter-branch', '-f', '--msg-filter', `sed "s|^|${root}: |"`,
      `temp-branch_start__${root}..temp-branch_end__${root}`]);
  }

  // // clear branches
  // for (let [root, hash] of Object.entries(root_commits)) {
  //   await git.raw(['branch', '-d', `temp-branch__${root}`]);
  // }

  
}
