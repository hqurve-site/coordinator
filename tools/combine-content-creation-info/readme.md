

# Initial merge
To combine the trees, we first run

```
git subtree add --prefix=main/ ../../site/content/main master
```
for each of the trees. For notes, we did something special since we wanted
the `components` directory to be on the root.
So, we instead

- `git subtree split --prefix=components -b temp_branch` (in the original notes repo)
- `git remote add notes-remote ../../site/content/notes`
- `git merge --allow-unrelated-histories notes-remote/temp_branch`

This gives us a basic merged repo.

# Rename commits (attempt)
The problem is that some of the git commits are not properly prefixed.
In particular, for the `[main,food,fun]` commits, we need to prefix `<repo>: ` to each commit.

# Actual final

After trying to do it programatically at first, I decided to just do it before merging the trees.

We also use `git filter-repo` https://github.com/newren/git-filter-repo.

```bash
# create new repo and add base commit

# go to content/main
git checkout --detach
git filter-branch -f --msg-filter 'sed "s|^|main: |"' HEAD
git filter-repo --force --to-subdirectory-filter main
git checkout -b reformatted_main

# go to food repo
git checkout --detach
git filter-branch -f --msg-filter 'sed "s|^|food: |"' HEAD
git filter-repo --force --to-subdirectory-filter food
git checkout -b reformatted_notes

# go to fun repo
git checkout --detach
git filter-branch -f --msg-filter 'sed "s|^|fun: |"' HEAD
git filter-repo --force --to-subdirectory-filter fun
git checkout -b reformatted_fun

# go to notes repo
git checkout --detach
git subtree split --prefix=components
git checkout -b reformatted_notes

# go back to new contents repo
git remote add old_main ../../site/content/main/
git remote add old_food ../../site/content/food/
git remote add old_fun ../../site/content/fun/
git remote add old_notes ../../site/content/notes/
git fetch --all

# the commits heads for the different trees are manually inspected
commits="old_main/reformatted_main old_food/reformatted_food old_fun/reformatted_fun old_notes/reformatted_notes"

# ensure that the commits for each tree are in order by date
for githash in $(echo $commits); do
    # https://stackoverflow.com/questions/53687124/bash-check-if-file-is-sorted-with-sort-c-file
    git log $githash --format="%at%H" --reverse | sort -C file || echo -n "not " ; echo "sorted"
done
# based on https://stackoverflow.com/questions/7575367/combining-unrelated-git-repositories-retaining-history-branches
# git log [unixtimestamp]
echo $commits | xargs -n 1 git log --format="%at %H" | sort | cut -c12- | xargs -n 1 git cherry-pick
```

Other things to note
- I had to ensure that the commits were in order. There were two that were out of order. To fix this, i editted
the commit in lazygit and then used the command `git commit --date="Fri Oct 15 08:30:00 2021 -0400" --amend` and then
ran `git rebase continue`

