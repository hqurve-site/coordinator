const fse = require('fs-extra');
const simpleGit = require('simple-git');

let list_map = new Map(require('./list.js'));

main().then(() => process.exit())

async function main() {
  await fse.mkdir('temp');
  const git = simpleGit({
    baseDir: process.cwd() + '/temp',
  });

  await git.init();
  await fse.outputFile('temp/.gitignore', '');
  await git.add(['.gitignore']);
  await git.commit('init');
  
  for (let [name, url] of list_map) {
    console.log(`adding Remote ${name}`);
    await git.addRemote(`origin-${name}`, url);
  }

  console.log('fetching');
  await git.fetch(['--all']);
  console.log('fech complete');

  for (let name of list_map.keys()) {
    console.log(`running ${name}`);
    await git.checkout(`origin-${name}/master`);
    await git.branch([`${name}-master`]);
    await git.checkout(`${name}-master`);

    await git.raw(['filter-branch', '-f', '--msg-filter', `sed "s|^|${name}: |"`, 'HEAD']);
    await git.raw(['filter-branch', '-f', '--tree-filter', `mkdir components && mkdir components/${name} && mv $(ls | rg --invert-match components) components/${name}`, 'HEAD']);

    try {
      await git.checkout(`origin-${name}/auto`);
      await git.branch([`${name}-auto`]);
      await git.checkout(`${name}-auto`);

      await git.raw(['filter-branch', '-f', '--msg-filter', `sed "s|^|${name}: |"`, 'HEAD']);
      await git.raw(['filter-branch', '-f', '--tree-filter', `mkdir components && mkdir components/${name} && mv $(ls | rg --invert-match components) components/${name}`, 'HEAD']);
    }catch(e) {
      console.log(e);
    }
  }
  console.log("done");
}
