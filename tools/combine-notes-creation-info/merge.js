
const fse = require('fs-extra');
const simpleGit = require('simple-git');

let list_map = new Map(require('./list.js'));

main().then(() => process.exit())

async function main() {
  const git = simpleGit({
    baseDir: process.cwd() + '/temp',
  });

  for (let branch of ['master', 'auto']) {
    let log_list = [];
    for (let name of list_map.keys()) {
      try {
      await git.checkout(`${name}-${branch}`);
      let log = await git.log();
      log_list.push(...log.all.map(l => {
        return {hash: l.hash, date: new Date(l.date), raw_date: l.date, message: l.message, component: name};
      }).reverse());
      }catch(e) {
        console.log(`Failed to checkout ${name}-${branch}. Skipping`);
      }
    }

    // Only sort if in different components otherwise keep order
    log_list.sort((a, b) => {
      if (a.component === b.component) return 0;
      return a.date-b.date;
    });

    await git.checkout('d76b2e6f669f9655dfc460555cc451a8dace965d'); // base commit (hardcoded because ...)
    await git.branch([`${branch}-temp0`]);
    await git.checkout([`${branch}-temp0`]);
    console.log(`starting cherrypick loop for ${branch}-temp0`);
    for (let {hash} of log_list) {
      await git.raw(['cherry-pick', hash]);
    }
    console.log(`ended cherrypick loop for ${branch}-temp0`);
  }
}
