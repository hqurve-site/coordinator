
## Steps

1. Create a repository (maybe also do a init commit)
2. Run `fetch-all.js` (depends on `rg`). This downloads all the repositories and mangles their commit names and directory structure
for combining. At this point, it might make sense to make a backup since this step is slow.
3. Run `merge.js`. This takes all the commits, sorts them by date and then cherry picks them into the relevant branch
4. Create a separate repository by cloning this one and rename commits. Eg `git clone /path/to/previous/repo --branch <branch> /path/to/output`.
We do this since the previous steps create lots of unwanted branches and commits. Finally, you can remove the local remote and do as you please.

Perhaps you would also want to conceal your actions (mainly because gitlab displays commit date instead of author date).
From `jsphpl` on [Stack overflow](https://stackoverflow.com/a/38586928)

```
git filter-branch --env-filter 'export GIT_COMMITTER_DATE="$GIT_AUTHOR_DATE"'
```
