let {filterLatex} = require('./util/latex');

let katex = require('katex');

function render(text, opts = {}) {
  text = text
    .replace(/\\DeclareMathOperator\{(.*?)\}\{(.*?)\}/g, "\\def$1{\\operatorname{$2}}")
    .replace(/\\DeclareMathOperator(\\[a-zA-Z]+)\{(.*?)\}/g, "\\def$1{\\operatorname{$2}}")
  ;
  // if (text.includes("vec")) console.log({text})

  let rendered = katex.renderToString(text, {
    // throwOnError: false,
    output: 'html',
    // from https://github.com/KaTeX/KaTeX/issues/2003#issuecomment-1186151412
    trust: context => ["\\htmlId", "\\href"].includes(context.command),
    // strictness refers to faithfulness to latex. Not safety
    strict: false,
    ...opts
  });

  return `<!-- \n${text}\n -->\n${rendered}`;
}

module.exports.register = function(registry, context) {
  registry.treeProcessor(function(){ 
    const self = this;
    this.process( doc => processDoc(self, doc));
  });
}

function processDoc(processor, doc) {
  let latex_prelude = [
    "page-latex-prelude-prefix", "page-latex-prelude"
  ].map(x => doc.getAttributes()[x]).filter(x => x!== undefined).join("\n").split(/\s*\n\s*/).filter(x => x !== "");
  let prelude = latex_prelude.length === 0 ? "" : latex_prelude.join('\n') + "\n";

  // find equation numbers
  // In first pass, we find all the labels, add numbers and store for later search
  let known_tags = {};
  let counter = 0;
  filterLatex(doc,
    // only handle stem block. It should be the only thing with equations
    (lines, doc) => lines.map(line =>
      line.replaceAll(/\\numberandlabel\{(.*?)\}/g, (match, g1) => {
        counter++; // increment counter
        if (known_tags[g1]) {
          console.log(`Warning: duplicate label: "${g1}"`);
        }
        known_tags[g1] = counter;
        return `\\htmlId{${g1}}{}\\tag{${counter}}`;
      })
    ),
  );
  // now set equation numbers
  /** @type{function(string): string} */
  let replace_eqref = text => text.replaceAll(
    /\\eqref\{(.*?)\}/g,
      (match, g0) => {
      let tag = known_tags[g0];
      if (tag) {
        return `\\href{#${g0}}{(${tag})}`
      }else{
        return `\\href{#}{(???)}` // missing
      }
    }
  );
  filterLatex(doc,
    (lines, doc) => lines.map(line => replace_eqref(line)),
    (kw, ext, text) => `${kw}:${ext}[${replace_eqref(text)}]`,
  );

  // perform latex conversion
  filterLatex(doc,
    (lines, block) => {
      let parent = block.parent;

      let html = render([...latex_prelude, ...lines].join('\n'), {displayMode: true});

      let newBlock = processor.$create_pass_block(block.parent,
        `<div class="stemblock">${html}</div>`,
      );
      parent.blocks[parent.blocks.indexOf(block)] = newBlock;
      return null;
    },
    (kw, ext, text) => {
      text = prelude + text.replace(/\\]/g, ']');
      return `pass:[<span class="steminline">\n${render(text).replaceAll("]", "\\]")}\n</span>]`;
    }
  );
}
