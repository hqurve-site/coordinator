module.exports.register = function(registry, context) {
  registry.treeProcessor(function() {
    const self = this

    self.process(doc => {
      // handle proofs
      doc.findBy({context: 'example', role: 'proof'}, example => {
        // adjust the title
        let title = example.title; // get raw string
        if (typeof title === "string") {
          example.setTitle('Proof: ' + title);
        }else{
          example.setTitle('Proof');
        }
        // make open collapsible if not collapsible
        if (!example.isOption('collapsible')) {
          example.setOption('collapsible');
          example.setOption('open');
        }
      });
      // handle others
      for (let role of ['theorem', 'proposition', 'corollary', 'lemma']) {
        let label = role[0].toUpperCase() + role.substring(1);
        doc.findBy({context: 'sidebar', role: role}, sidebar => {
          // adjust the title
          let title = sidebar.title; // get raw title
          if (typeof title === "string") {
            if (!title.toLowerCase().startsWith(role)) {
              // only manipulate if it does not start with role
              sidebar.setTitle(`${label}: ${title}`);
            }
          }else{
            sidebar.setTitle(label);
          }
          // add extra role for easier css
          sidebar.addRole('theoremblock');
        })
      }
    });
  })
}
