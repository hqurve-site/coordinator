// we need to perform some transformations to the latex for mathjax
// - add equation numbers
// References:
// https://docs.mathjax.org/en/latest/input/tex/extensions/html.html

const { filterLatex } = require("./util/latex");


module.exports.register = function(registry, context) {
  registry.treeProcessor(function(){ 
    const self = this;
    this.process( doc => addEquationNumbers(self, doc));
  });
}

// This is based on https://github.com/KaTeX/KaTeX/issues/2003
// We are looking for macros called \\numberandlabel{label} and \\eqref{label}
function addEquationNumbers(processor, doc) {
  // in first pass, we find all the labels, add numbers and store for later search
  let known_tags = {};
  let counter = 0;

  filterLatex(doc,
    // only handle stem block. It should be the only thing with equations
    (lines, doc) => lines.map(line =>
        line.replaceAll(/\\numberandlabel\{(.*?)\}/g, (match, g1) => {
          counter++; // increment counter
          if (known_tags[g1]) {
            console.log(`Warning: duplicate label: "${g1}"`);
          }
          known_tags[g1] = counter;
          return `\\cssId{${g1}}{\\tag{${counter}}}`;
        })
    ),
  );

  // next, find all eqref and replace it
  /** @type{function(string): string} */
  let replace_eqref = text => text.replaceAll(
    /\\eqref\{(.*?)\}/g,
      (match, g0) => {
      let tag = known_tags[g0];
      if (tag) {
        return `\\href{#${g0}}{(${tag})}`
      }else{
        return `\\href{#}{(???)}` // missing
      }
    }
  );
  filterLatex(doc,
    (lines, doc) => lines.map(line => replace_eqref(line)),
    (kw, ext, text) => `${kw}:${ext}[${replace_eqref(text)}]`,
  );
}
