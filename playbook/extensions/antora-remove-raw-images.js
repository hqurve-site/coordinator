// @ts-check
// remove anything that is under "*/_raw_images"

let filters = [
  /\/_raw_images\//
];

/** @type AntoraExtensionRegister */
module.exports.register = (context) => {
  context.on('contentAggregated', ({siteCatalog, contentAggregate}) => {
    for (let entry of contentAggregate) {
      entry.files = entry.files.filter(file => !filters.some(r => r.test(file.src.path)));
    }
  });
};
