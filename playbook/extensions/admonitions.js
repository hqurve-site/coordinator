const custom = [
  {name: 'remark', label: 'Remark'},
  {name: 'idea', label: 'Idea'},
  {name: 'thought', label: 'Thought'},
  {name: 'todo', label: 'Todo'},
]
module.exports.register = function(registry, context){
  for (const {name, label} of custom){
    registry.block(function(){
      const self = this;
      self.named(name.toUpperCase());
      self.onContexts('example', 'paragraph');

      self.process((parent, reader, attrs) =>{
        attrs.name = name;
        attrs.textlabel = label;
        attrs.style = name.toUpperCase();

        // Inspired by https://github.com/asciidoctor/asciidoctor-pdf/blob/544e925f69c1348c8ab7eb50c80fd2324f2d19f7/spec/admonition_spec.rb#L824-L859
        // as well as documentation https://asciidoctor.github.io/asciidoctor.js/2.2.5/#extensionsprocessorcreateblock
        const opts = {
          content_model: 'compound'
        };
        const lines = reader.getLines();

        return self.createBlock(parent, 'admonition', lines, attrs, opts);
      });
    });
    registry.treeProcessor(function(){
      const self = this;
      self.process( doc =>{
        doc.findBy({context: 'paragraph'}, para =>{
          const lines = para.lines;
          if (lines.length > 0 && lines[0].startsWith(`${name.toUpperCase()}: `)){
            const blocks = para.parent.blocks;
            const index = blocks.indexOf(para);

            const attrs = para.getAttributes();
            attrs.name = name;
            attrs.textlabel = label;
            attrs.style = name.toUpperCase();

            lines[0] = lines[0].substring(name.length + 2);

            const opts = {
              content_model: 'compound'
            };

            // Thanks to ggrossetie who introduced me to the parseContent method
            // https://asciidoctor.zulipchat.com/#narrow/stream/279642-users/topic/Extension.3A.20Empty.20block.20created.20within.20tree.20processor/near/268840937
            blocks[index] = self.createBlock(para.parent, 'admonition', [], attrs, opts);
            self.parseContent(blocks[index], lines);
          }
        });
      })
    });
  };
};

