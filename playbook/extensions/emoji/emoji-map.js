// Exports an object with (name -> string)

// CLDR annotations-full version 41
// Map taken from https://github.com/unicode-org/cldr-json/blob/main/cldr-json/cldr-annotations-full/annotations/en/annotations.json
// which I obtained by first looking at https://cldr.unicode.org/index/downloads then http://unicode.org/Public/cldr/41/
let annotations = require('./annotations.json').annotations.annotations;

function warn_init(str) {
  console.log(str);
}

function construct_map() {
  let initial_map = {}; // map of keyword -> {primary: <symbol>, others: [[<symbol>, index]]

  function get_primary(keyword){
    if (initial_map[keyword] === undefined) {
      initial_map[keyword] = {};
    }
    if (initial_map[keyword].primary !== undefined) {
      warn_init(`primary already stored for ${keyword}`);
    }
    return initial_map[keyword];
  }
  for (let [symbol, v] of Object.entries(annotations)) {
    let primary = v.tts;
    let others = v.default;

    get_primary(primary).primary = symbol;

    for (let i=0; i < others.length; i++) {
      let keyword = others[i];
      
      if (initial_map[keyword] === undefined) initial_map[keyword] = {};
      if (initial_map[keyword].others === undefined) initial_map[keyword].others = [];

      initial_map[keyword].others.push([symbol, i]);
    }
  }
  let final_map = {};
  for (let [keyword, v] of Object.entries(initial_map)) {
    let primary = v.primary;
    let others = v.others;

    keyword = keyword.replaceAll(' ', '_');

    if (primary !== undefined) {
      final_map[keyword] = primary;
    }else{
      let min = others.reduce((a,b) => {
        if (b[1] < a[1]) return b;
        return a;
      });
      final_map[keyword] = min[0];
    }
  }
  return final_map;
}

module.exports = construct_map();
