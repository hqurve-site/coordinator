
// name is a string containing 'A-Za-z0-9_-'
// 
// marcos are interpretted from the outside inward to avoid surprises. That is in
//    call::abba[call::bba[]]
// call::abba gets interpretted first. This also allows for some very stupid things 
// to occur. For example, the inner macro could only be "partially applied"

const wordRegex = /[\w-]+/

const knownPairs = [
  [['[', ']'], ['\\[', '\\]']],
  [['(', ')'], ['\\(', '\\)']],
  [['{', '}'], ['{', '}']]
];
function getDelimPair(string){
  for (const [[a, b], [ar, br]] of knownPairs){
    if (string.startsWith(a)){
      return [ar,br];
    }
  }
  return [string[0], string[0]];
}

// all return [skip, newString]
//
function parseDef(string, macros){
  const orig = string;

  let name = string.match(wordRegex)[0];
  string = string.slice(name.length);
  
  let delims = getDelimPair(string);
  let count = string.match(new RegExp(`${delims[0]}(\\d+)${delims[1]}`));
  string = string.slice(count[0].length);
  count = count[1];

  delims = getDelimPair(string);
  let sub = string.match(new RegExp(`${delims[0]}(.*?)(?<!\\\\)${delims[1]}`));
  string = string.slice(sub[0].length);
  sub = sub[1];

  if (!macros.has(name)) macros.set(name, []);
  macros.get(name).unshift({paramCount: count, sub: sub});

  return [orig.length - string.length, ''];
}

function parseUndef(string, macros){
  const orig = string;

  const name = string.match(wordRegex)[0];
  macros.get(name).shift();
  string = string.slice(name.length);

  return [orig.length - string.length, ''];
}
function parseCall(string, macros){
  const orig = string;

  let name = string.match(wordRegex)[0];
  string = string.slice(name.length);

  let macro = macros.get(name)[0];
  let newString = macro.sub;

  for (let i =0; i < macro.paramCount; i++){
    const delims = getDelimPair(string);
    let param = string.match(new RegExp(`${delims[0]}(.*?)(?<!\\\\)${delims[1]}`));
    string = string.slice(param[0].length);
    param = param[1];

    newString = newString.replace(new RegExp(`\\\\${i}`, 'g'), param);
  }

  return [orig.length - string.length, newString]
}

module.exports.register = function(registry, context){
  registry.preprocessor(function(){
    var self = this;
    self.process(function(doc, reader){
      
      const linefeed = '\n';


      const processors = [
        [`(?<=\n)${linefeed}def::`, parseDef],
        [`(?<=\n)${linefeed}undef::`, parseUndef],
        [`call::`, parseCall]
      ].map(([r,f]) => [new RegExp(r), f]);

      const macros = new Map();
      
      var string = reader.getString();
      var index = 0;
      while (true){
        const pair = processors
          .map( ([regex, f]) => {
            const match = string.slice(index).match(regex);
            if (match == null) return null;
            return [match, f];
          })
          .filter(x => x != null)
          .reduce((prev, curr) => {
            if (prev == null || prev[0].index > curr[0].index){
              return curr;
            }else{
              return prev;
            }
          }, null);

        if (pair == null) break;


        const [match, f] = pair;

        index = index + match.index;

        const start = index + match[0].length;
        const [skip, newString] = f(string.slice(start), macros, match);
        
        string = string.slice(0, index) + newString + string.slice(start + skip);

      }
      reader.lines = string.split(linefeed).reverse();
      return reader;
    });
  });
};
