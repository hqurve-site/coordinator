// simply add the WRITING_GUIDE file to develop@main:ROOT:writing-guide.adoc
// Read file based on https://gitlab.com/antora/antora/-/blob/b5d124d79316c1b34f5c260aa4166f086325ca11/packages/content-aggregator/lib/aggregate-content.js#L496

const fs = require('fs');
const path = require('path');

let FILES = [
  {component: "main", version: "develop", path: "modules/ROOT/pages/writing-guide.adoc", origin_path: "../WRITING_GUIDE.adoc"}
];

module.exports.register = (context) => {
  context.on('contentAggregated', ({contentAggregate}) => {
    // for each component
    for (let component of contentAggregate) {
      // for each file
      for (let {component: component_name, version, path: out_path, origin_path} of FILES) {
        if (component.name === component_name && component.version === version) {
          // add file
          let contents = fs.readFileSync(path.join(__dirname, origin_path))
          const stat = Object.assign(new fs.Stats(), { mode: 0o7777, mtime: undefined, size: contents.byteLength })
          let file = new File({
            path: out_path,// path in the component,
            contents: contents,
            stat: stat,
          });
          assignFileProperties(file, component.files[0].src.origin);
          component.files.push(file);
        }
      }
    }
  });

}

// all below copied from @antora/content-aggregator

const Vinyl = require('vinyl')

class File extends Vinyl {
  get path () {
    return this.history[this.history.length - 1]
  }

  set path (path_) {
    this.history.push(path_)
  }

  get relative () {
    return this.history[this.history.length - 1]
  }
}

function assignFileProperties (file, origin) {
  if (!file.src) file.src = {}
  Object.assign(file.src, { path: file.path, basename: file.basename, stem: file.stem, extname: file.extname, origin })
  if (origin.fileUriPattern) {
    const fileUri = origin.fileUriPattern.replace('%s', file.src.path)
    file.src.fileUri = ~fileUri.indexOf(' ') ? fileUri.replace(SPACE_RX, '%20') : fileUri
  }
  if (origin.editUrlPattern) {
    const editUrl = origin.editUrlPattern.replace('%s', file.src.path)
    file.src.editUrl = ~editUrl.indexOf(' ') ? editUrl.replace(SPACE_RX, '%20') : editUrl
  }
  return file
}
