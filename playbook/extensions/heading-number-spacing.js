const tags = [
  "h2"
  ,"h3"
  ,"h4"
];
const tag_regex = `(${tags.join("|")})`;

module.exports.register = function(registry, context) {
  registry.postprocessor(function(){
    const self = this;
    self.process(function (doc, output) {
      return output.replace(
        new RegExp(`(?<=^<(${tag_regex}).*?><a.*?></a>)(?<number>\\d+(\\.\\d+)*)\\.\\s*`, 'gm'),
        '<span class="number">$<number> </span>' // that space is important
      );
    });
  });
};
