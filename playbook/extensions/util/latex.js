module.exports.filterLatex = filterLatex;
// ideas for where to process came from https://gitlab.com/djencks/asciidoctor-mathjax.js/-/blob/main/lib/asciidoctor-mathjax.js
// which itself came from asciidoctor-mathematical
/**
  * @param {object} doc
  * @param {null | (lines: [string], block: object) => [string] | null} block_handler - Returns new lines otherwise null if block was modified by function
  * @param {null | (keyword: string, extra: string, inner: string) => string} inline_handler - Returns full text to replace
  */
function filterLatex (doc, block_handler, inline_handler) {
  block_handler = block_handler || (() => {}); // do nothing
  inline_handler = inline_handler || ((kw, ext, inner) => `${kw}:${ext}[${inner}]`);

  let processInlineStem = text => {
    // <keyword>:<maybe other words>[<inner text>]
    // inner text: greedy one of { \], anything except ] }
    // If there is a leading \, we do nothing
    let StemInlineMacroRegex = /\\?(stem|asciimath|latexmath):([a-z,]*)\[((\\\]|[^\]])*)\]/g;

    let newText = text.replace(StemInlineMacroRegex, (match, g0, g1, g2, g3, index, string) => {
      if (match.startsWith('\\')) {
        return match;
      }else{
        return inline_handler(g0,g1,g2);
      }
    });
    return newText;
  }
  doc.findBy({}, block => {
    // handle stem block
    if (block.context === 'stem') {
      let output = block_handler(block.lines, block);
      if (output !== null) {
        block.lines = output;
      }
    }

    // handle headers
    if (typeof block.title === 'string') {
      block.title = processInlineStem(block.title)
      delete block.converted_title;
    }

    // handle paragraphs
    if (block.context === 'paragraph') {
      let text = block.lines.join('\n');
      text = processInlineStem(text);
      block.lines = text.split('\n');
    }

    if (block.context === 'admonition') {
      let text = block.lines.join('\n');
      text = processInlineStem(text);
      block.lines = text.split('\n');
    }

    // handle .... not too sure
    if (block.content_mode === 'simple' && block.subs.indexOf('macro') && block.context !== 'table_cell') {
      console.log(block);
      process.exit(1);
    }

    // handle list
    if (block.context === 'list_item' && typeof block.text === 'string') {
      block.text = processInlineStem(block.text);
    }

    // handle table_cells
    if (block.context === 'table_cell') {
      if (block.style === 'asciidoc') {
        filterLatex(block.$inner_document(), block_handler, inline_handler);
      }else if(block.style !== 'literal') {
        block.text = processInlineStem(block.text);
      }
    }
  });
}
