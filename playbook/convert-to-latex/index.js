// @ts-check

const assert = require('node:assert').strict; // https://nodejs.org/api/assert.html

const OUTPUT_PREFIX = "__convert-to-latex/"

// template file with
// "%%INSERT CONTENT HERE"
const TEMPLATE_FILE = (() => {
  const fs = require('fs');
  const path = require('path');
  return fs.readFileSync(path.join(__dirname, 'template.tex'), {encoding: 'utf-8'})
})()


// ENTRY point
/** @type AntoraExtensionRegister */
module.exports.register = (context) => {
  context.on('contentAggregated', ({siteCatalog, contentAggregate}) => {
    // determine layout
    let layout = determineLayout(context, contentAggregate);
    siteCatalog.addFile({
      contents: Buffer.from(JSON.stringify(layout)),
      out: {path: `${OUTPUT_PREFIX}debug/layout.json`}
    });

    // determine the content. This is a map from the resource identifier version@component:module:file.adoc
    // to {version, component, module, file, document, asciidoc_confg}
    let content = parsePages(context, contentAggregate, layout);

    // convert each component to latex
    for (let component of layout) {
      let { latex } = convertComponentToLatex(context, component, content);
      siteCatalog.addFile({
        contents: Buffer.from(latex),
        out: {path:`${OUTPUT_PREFIX}latex/${component.component}--${component.version}.tex`}
      })
    }

    // convert each page to latex
    let latex_pages = parseConvertSinglePagesToLatex(context, contentAggregate);
    /** @type Object<string, string> */
    let map = {}
    for (let {filespec, latex} of latex_pages) {
      let path = filespec.filename.replace(/\.adoc$/, "");
      let filename = `${filespec.component}--${filespec.version}--${filespec.module}--${path.replaceAll("/", "__")}`;
      let destination;
      if (filespec.module == "ROOT") {
        destination = `${filespec.component}/${filespec.version}/${path}`;
      }else{
        destination = `${filespec.component}/${filespec.version}/${filespec.module}/${path}`;
      }
      siteCatalog.addFile({
        contents: Buffer.from(latex),
        out: {path: `${OUTPUT_PREFIX}page-latex/${filename}.tex`}
      })

      map[filename + ".pdf"] = destination + ".pdf";
    }
    // also create a file map
    siteCatalog.addFile({
        contents: Buffer.from(
          "First line to be ignored. Contains pairs of line of the form <relative filename> <destination relative to root>"
          + Object.entries(map).sort().map(([k, v]) => `\n${k}\n${v}`).join("")
        ),
        out: {path: `${OUTPUT_PREFIX}page-latex-map.txt`}
    });

    // add assets to output
    addAssetsToOutput(context, contentAggregate, siteCatalog);
  });
}

// Stage 1: Layout
// Uses the component descriptors to determine in what order the files should be laid out
/**
  * @param {AntoraContext} context
  * @param {AntoraContentAggregate} contentAggregate
  * @returns {Array<ComponentLayout>} an array where each entry corresponds to a component
  */
function determineLayout(context, contentAggregate) {
  let logger = context.getLogger('convert-to-latex');

  let output = []
  // process each version separately
  for (let component of contentAggregate) {
    // first, get a list of nav files for the component

    if (! component.book?.layout) {
      logger.warn(`Missing book.layout for ${component.version}@${component.name}`) // TODO: change to error once working
      continue; // skip component
    }

    // layout as defined in doc above
    /** @type Array<ComponentLayoutEntry> */
    let layout = []
    // See Writing guide for format for book.layout
    for (let entry of component.book.layout) {
      // IMPORTANT: Very strange: the parsed yaml converts
      // snake case (with underscores/hyphens) to camel case
      
      // if corresponds to file
      if (entry.src) {
        // ensure src contains :
        if (! entry.src.includes(':')) {
          logger.error(`Incorrect format for component.book.layout src "${entry.src}"`)
        }
        let [mod, filename] = entry.src.split(':', 2);
        layout.push({
          type: "file",
          title: entry.title,
          title_level: entry.titleLevel ?? entry.level ?? 1,
          content_level: entry.contentLevel ?? entry.level ?? 1,
          file_spec: {
            component: component.name,
            version: component.version,
            module: mod,
            filename: filename,
          },
        })
        // parse src
      }else{
        // otherwise, just a title
        assert(entry.title != undefined && entry.titleLevel != "omit");
        layout.push({
          type: "title",
          title: entry.title,
          title_level: entry.titleLevel ?? entry.level ?? 1,
        })
      }
    }

    output.push({
      title: component.book.title ?? component.title,
      component: component.name,
      version: component.version,
      single_chapter: component.book.singleChapter ?? false,
      layout
    })
  }

  return output;
}

// Stage 2: Parse asciidoc pages
// Convert and organize asciidoc files in the contentAggregate.
// Returns a dictionary whose entries are
// "version@component:module:filename" -> {filename, module, component, version, document, asciidoc_config, content_level}
/**
  * @param {AntoraContext} context
  * @param {AntoraContentAggregate} contentAggregate
  * @param {Array<ComponentLayout>} layout
  * @return {Object<string, PageSpec>}
  */
function parsePages(context, contentAggregate, layout) {
  // First, collect all files. This is needed to work with includes
  // This is a list of {path, component, version, contents: string, asciidoc_config}
  // Asciidoc is the set of asciidoc settings. This is useful when parsing latex
  // The key is version@component:path
  let collected = Object.fromEntries(contentAggregate.flatMap(component => component.files)
    .map(file => {
      let out = {
        path: file.src.path,
        component: file.src.origin.descriptor.name,
        version: file.src.origin.descriptor.version,
        asciidoc_config: file.src.origin.descriptor.asciidoc,
        contents: file._contents
      };

      return [
        `${out.version}@${out.component}:${out.path}`,
        out,
      ]
    })
  ); // end Object.fromEntries

  const loadAsciidoc = require('./load-asciidoc');

  // contains dictionary in output format
  /** @type Object<string, PageSpec> */
  let output = {}
  // parse pages files only
  for (let entry of Object.values(collected)) {
    // try match modules/<module>/pages/filename
    let result = /modules\/([\w-]+)\/pages\/(.*\.adoc)/.exec(entry.path);
    if (!result) continue;

    let entry2 = {
      ...entry,
      module: result[1],
      filename: result[2],
    };

    // find which component it is in so that we can find the content_level
    /** @type ComponentLayoutEntryPage | undefined */
    // @ts-ignore
    let component_entry = layout.flatMap(l => l.layout).find(e => {
      // console.log(e);
      return (e.type === "file"
        && e.file_spec.component === entry2.component 
        && e.file_spec.version === entry2.version 
        && e.file_spec.module === entry2.module
        && e.file_spec.filename === entry2.filename
      );
    });

    if (component_entry?.content_level === undefined) {
      // let contents = entry.contents;
      // delete entry.contents; // clear for printing
      // context.getLogger('convert-to-latex').warn(`Unable to find component for ${JSON.stringify(out)}`)
      // entry.contents = contents;
      continue;
    }

    let entry3 = {
      ... entry2,
      content_level : component_entry?.content_level, // store in entry since it is needed to load the document
    };

    // load document
    let document = loadAsciidoc(context, entry3, collected);
    let out = {
      component: entry3.component,
      version: entry3.version,
      module: entry3.module,
      filename: entry3.filename,
      content_level: entry3.content_level,
      asciidoc_config: entry.asciidoc_config,
      document: document
    };

    output[`${out.version}@${out.component}:${out.module}:${out.filename}`] = out;
  }

  return output;
}

// Stage 3 convert to latex
module.exports.convertComponentToLatex = convertComponentToLatex;
/**
  * @param {AntoraContext} context
  * @param {ComponentLayout} component is a single object generated by determineLayout (stage 1)
  * @param {Object<string, PageSpec>} content is generated by parsePages (stage 2)
  * @returns {{latex: string}} Note that we just use an object to make later modifications easier
  */
function convertComponentToLatex(context, component, content) {
  let {title, body} = require('./convert').convert_component(context, component, content);

  /** @type string */
  let document_class;
  if (component.single_chapter) {
    document_class = "\\documentclass{article}";
  }else{
    document_class = "\\documentclass{book}";
  }

  let prelude = `\\title{${title}}`;

  return {
    latex: TEMPLATE_FILE
    .replace("%% DOCUMENT CLASS HERE", () => document_class)
    .replace("%% INSERT CONTENT HERE", () => body)
    .replace("%% PRELUDE CONTENT HERE", () => prelude),
  };
}

// Stage 3.5 convert each page to latex
// For each asciidoc file in the contentAggregate, convert to latex
/**
  * @param {AntoraContext} context
  * @param {AntoraContentAggregate} contentAggregate
  * @return {Array<{filespec: FileSpec, latex: string}>}
  */
function parseConvertSinglePagesToLatex(context, contentAggregate) {
  let logger = context.getLogger('convert-to-latex');
  // First, collect all files. This is needed to work with includes
  // This is a list of {path, component, version, contents: string, asciidoc_config}
  // Asciidoc is the set of asciidoc settings. This is useful when parsing latex
  // The key is version@component:path
  let collected = Object.fromEntries(contentAggregate.flatMap(component => component.files)
    .map(file => {
      let out = {
        path: file.src.path,
        component: file.src.origin.descriptor.name,
        version: file.src.origin.descriptor.version,
        asciidoc_config: file.src.origin.descriptor.asciidoc,
        contents: file._contents
      };

      return [
        `${out.version}@${out.component}:${out.path}`,
        out,
      ]
    })
  ); // end Object.fromEntries

  const loadAsciidoc = require('./load-asciidoc');

  /** @type Array<{filespec: FileSpec, latex: string}> */
  let output = []

  for (let entry of Object.values(collected)) {
    // try match modules/<module>/pages/filename
    let result = /modules\/([\w-]+)\/pages\/(.*\.adoc)/.exec(entry.path);
    if (!result) continue;
    // if (entry.component != "math6620") continue;

    let entry2 = {
      ...entry,
      module: result[1],
      filename: result[2],
      content_level: 1
    };

    let /** @type string */ title, /** @type string */ body;
    try {
      let document = loadAsciidoc(context, entry2, collected);
      ({title, body} = require("./convert").convert_single_page(context, {
        filename: entry2.filename,
        module: entry2.module,
        component: entry2.component,
        version: entry2.version,
        asciidoc_config: entry2.asciidoc_config,
        content_level: entry2.content_level,
        document
      }));
    }catch (e) {
      entry2.contents = Buffer.from("");
      entry2.asciidoc_config = {attributes: {}};
      logger.error(`Error while parsing ${JSON.stringify(entry2)}`);
      throw e;
    }

    let document_class = "\\documentclass{article}";

    let prelude = `\\title{${title}}\n\\def\\asciidocsinglepage{}`;

    output.push( {
      filespec: {
        filename: entry2.filename,
        module: entry2.module,
        component: entry2.component,
        version: entry2.version,
      },
      latex: TEMPLATE_FILE
      .replace("%% DOCUMENT CLASS HERE", () => document_class)
      .replace("%% INSERT CONTENT HERE", () => body)
      .replace("%% PRELUDE CONTENT HERE", () => prelude),
    });
  }

  return output
}


// State 4 add additional assets
// This function places the assets directly into the output
/**
  * @param {AntoraContext} context
  * @param {AntoraContentAggregate} contentAggregate
  * @param {AntoraSiteCatalog} siteCatalog
  */
function addAssetsToOutput(context, contentAggregate, siteCatalog) {
  for (let component of contentAggregate) {
    let component_name = component.name;
    let version = component.version;

    for (let file of component.files) {
      let src_path = file.src.path;

      let result = /modules\/([\w-]+)\/([\w-]+)\/(.*)/.exec(src_path);

      if (!result) continue;

      let module = result[1];
      let family = result[2];
      let filename = result[3];

      let mapped_family = {
        'images': 'images'
      }[family];

      if (mapped_family) {
        let out_path = `${mapped_family}/${version}-${component_name}/${module}/${filename}`;


        siteCatalog.addFile({
          contents: file._contents,
          out: {path:`${OUTPUT_PREFIX}latex/${out_path}`}
        })
      }
    }
  }
}

