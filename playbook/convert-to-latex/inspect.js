const DEBUGPATH='__convert-to-latex/debug/'

module.exports.register = (context) => {
  // Inspect the contentAggregate
  context.on('contentAggregated', ({siteCatalog, contentAggregate}) => {
    for (let component of contentAggregate) {
      component = {...component} // do not modify internal structure
      component.files = component.files.map(file => {
        file = {...file} // do not modify internal structure
        if (file._contents) {
          file._contents = file._contents.toString(); // convert Buffer to string https://nodejs.org/api/buffer.html#buftostringencoding-start-end
        }

        return file;
      });
      siteCatalog.addFile({
        contents: Buffer.from(JSON.stringify(component)),
        out: {path: `${DEBUGPATH}contentAggregate.${component.name}-${component.version}.json` }
      })
    }
  });
  // Inspect the contentCatalog
  // https://gitlab.com/antora/antora/-/blob/3c15cf5f56f0172a17cb5f00edb975d40921de47/packages/asciidoc-loader/lib/load-asciidoc.js#L38
  context.on('contentClassified', ({siteCatalog, contentCatalog}) => {
    // see getFiles
    // let files = contentCatalog.getFiles().map(file => {
    //   file = {...file}; // do not modify initial structure
    //   // console.log(typeof file._contents);
    //   // console.log(file._contents);
    //   if (file._contents) {
    //     file._contents = (new TextDecoder('UTF-8')).decode(file._contents);
    //   }
    //   return file;
    // });
    // siteCatalog.addFile({
    //   contents: Buffer.from(JSON.stringify(files)),
    //   out: {path: "contentCatalog.getFiles.json"}
    // });
    //
    // // see getComponents
    // siteCatalog.addFile({
    //   contents: Buffer.from(JSON.stringify(contentCatalog.getComponents())),
    //   out: {path: "contentCatalog.getComponents.json"}
    // });

    // see what the buildCatalog function produces
    // let navBuilder = require('@antora/navigation-builder');
    // let navCatalog = navBuilder(contentCatalog);
    // let simpleCatalog = contentCatalog.getComponents().flatMap(c => c.versions).map(c => navCatalog.getNavigation(c.name, c.version));
    // siteCatalog.addFile({
    //   contents: Buffer.from(JSON.stringify(simpleCatalog)),
    //   out: {path: "navigation_catalog.json"}
    // })
  });
}
