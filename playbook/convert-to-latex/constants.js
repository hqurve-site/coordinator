'use strict'
// from https://gitlab.com/antora/antora/-/blob/main/packages/asciidoc-loader/lib/constants.js?ref_type=heads

module.exports = Object.freeze({
  $Antora () {},
  EXAMPLES_DIR_TOKEN: 'example$',
  PARTIALS_DIR_TOKEN: 'partial$',
});

