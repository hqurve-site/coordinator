
/** @typedef {object} FileSpec
  * @property {string} component
  * @property {string} version
  * @property {string} module
  * @property {string} filename
  */

/** @typedef {object} ComponentLayout
  * @property {string} title
  * @property {string} component
  * @property {string} version
  * @property {bool} single_chapter
  * @property {Array<ComponentLayoutEntry>} layout
  */
/** @typedef {ComponentLayoutEntryTitle|ComponentLayoutEntryPage} ComponentLayoutEntry */

/** @typedef {object} ComponentLayoutEntryTitle
  * @property {"title"} type
  * @property {number} title_level
  * @property {string} title
  */
/** @typedef {object} ComponentLayoutEntryPage
  * @property {"file"} type
  * @property {number | "omit"} title_level
  * @property {number} content_level
  * @property {string} [title] - optional title string
  * @property {FileSpec} file_spec
  */

/** @typedef {object} PageSpec
  * @property {string} filename
  * @property {string} module
  * @property {string} component
  * @property {string} version
  * @property {AsciidocDocument} document
  * @property {{attributes: object}} asciidoc_config
  * @property {number} content_level
  */
