const fs = require('fs');
const path = require('path');
const loadAsciidoc = require('./load-asciidoc')
const {convertComponentToLatex} = require('.');

main(process.argv.slice(2)); // should start with "node path/to/script", so we remove it
function main(args) {
  let context = createContextShim();
  let input = path.join(path.dirname(__dirname), "WRITING_GUIDE.adoc");
  let output = "build/convert-to-latex/example.tex";

  let file_spec = {
    component: "test",
    version: "develop",
    path: "modules/ROOT/pages/index.adoc",
    module: "ROOT",
    filename: "index.adoc",
    title_level: 2,
    content_level: 2,
    asciidoc_config: {},
  };
  let component = {
    title: "Test",
    component: "test",
    version: "develop",
    layout: [{type: "file", title_level: 2, content_level: 2, file_spec: {component: "test", version: "develop", module: "ROOT", filename: "index.adoc"}}]
  };

  for (let flag of args) {
    if (flag === "-h" || flag === "--help") {
      console.log(`
      Usage:
        -h --help          : show this message
        --input=[filename]  : convert the given file. If no path is given, we convert the WRITING_GUIDE.adoc file

        --output=[path]    : output path (default = build/convert-to-latex/writing_guide.tex)
      `)
      return;
    }else if (flag.startsWith('--input=')) {
      input = flag.substring("--input=".length);
    }else if (flag.startsWith('--output=')) {
      output = flag.substring("--output=".length);
    }else{
      // fallout
      console.warn("Use the -h flag to show help")
      return;
    }
  }

  // make output directory
  fs.mkdirSync(path.dirname(output), {recursive: true});

  // load file
  file_spec.contents = fs.readFileSync(input);

  // parse file
  file_spec.document = loadAsciidoc(context, file_spec, {});

  let content = Object.fromEntries([
    [`${file_spec.version}@${file_spec.component}:${file_spec.module}:${file_spec.filename}`, file_spec]
  ]);

  let { latex } = convertComponentToLatex(context, component, content);

  fs.writeFileSync(output, latex)


}


function createContextShim(){
  return context_shim = {
    getLogger: () => {
      return {
        info: console.log,
        warn: console.warn
      }
    }
  };
}
