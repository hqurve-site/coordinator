'use strict'

// from https://gitlab.com/antora/antora/-/blob/main/packages/asciidoc-loader/lib/include/resolve-include-file.js?ref_type=heads
// but adjusted to work with our catalog

const { posix: path } = require('path')
// const splitOnce = require('../util/split-once') // see below

const { EXAMPLES_DIR_TOKEN, PARTIALS_DIR_TOKEN } = require('../constants')
const RESOURCE_ID_DETECTOR_RX = /[$:@]/

/**
 * Resolves the specified target of an include directive to a virtual file in the content catalog.
 *
 * @memberof asciidoc-loader
 *
 * @param {Object} context - context of antora extensions
 * @param {String} target - The target of the include directive to resolve.
 * @param {Cursor} cursor - The cursor of the reader for file that contains the include directive.
 * @param {Object} file - {version, component, path, contents, asciidoc_config, content_level} // contents is a Buffer
 * @param {Object} version@component:path -> {version, component, path, content, asciidoc_config, content_level}
 * @returns {Object} A map containing the file, path, and contents of the resolved file. // contents is a string
 */
function resolveIncludeFile (context, target, cursor, file, catalog) {
  const getById = ({component, version, path}) => {
    let id = `${version}@${component}:${path}`;
    return catalog[id];
  };

  const src = cursor.src ?? file;

  let resolved
  let family
  let relative
  if (RESOURCE_ID_DETECTOR_RX.test(target)) { // checks if any part of target matches the detector
    // support for legacy {partialsdir} and {examplesdir} prefixes is @deprecated; scheduled to be removed in Antora 4
    if (target.startsWith(PARTIALS_DIR_TOKEN) || target.startsWith(EXAMPLES_DIR_TOKEN)) {
      ;[family, relative] = splitOnce(target, '$')
      if (relative.charAt() === '/') relative = relative.substring(1)
      resolved = getById({
        component: src.component,
        version: src.version,
        path: `modules/${src.module}/${family}/${relative}`
      })
    } else {
      // https://gitlab.com/antora/antora/-/blob/main/packages/content-classifier/lib/util/parse-resource-id.js?ref_type=heads
      let parseResourceId = context.require('@antora/content-classifier/util/parse-resource-id');

      // get module from src.path
      let module_ = /modules\/([\w-]+\/.*)/.exec(src.path);
      if (module_) {
        module_ = module_[1];
      }

      let resource_id = parseResourceId(target, parseResourceId(`${src.version}@${src.component}`));

      // if we have an invalid resource id, return
      if (!resource_id) return;
      
      resolved = getById({
        version: resource_id.version,
        component: resource_id.component,
        path: resource_id.path,
      })
    }
  } else {
    // bypassing resource ID resolution for relative include path is @deprecated; scheduled to be removed in Antora 4
    resolved = getById({
      version: src.version,
      component: src.component,
      path: path.join(path.dirname(src.path), target) // use path.join to avoid leading or trailing /
    })
  }
  if (resolved) {
    const resolvedSrc = resolved.src
    return {
      src: resolvedSrc,
      contents: resolved.contents.toString(),
    }
  }
}

module.exports = resolveIncludeFile


// from https://gitlab.com/antora/antora/-/blob/main/packages/asciidoc-loader/lib/util/split-once.js?ref_type=heads
/**
 * Splits the specified string at the first occurrence of the specified separator.
 *
 * @memberof asciidoc-loader
 *
 * @param {String} string - The string to split.
 * @param {String} separator - A single character on which to split the string.
 * @returns {String[]} A 2-element Array that contains the string before and after the separator, if
 * the separator is found, otherwise a single-element Array that contains the original string.
 */
function splitOnce (string, separator) {
  const separatorIdx = string.indexOf(separator)
  return ~separatorIdx ? [string.substring(0, separatorIdx), string.substr(separatorIdx + 1)] : [string]
}
