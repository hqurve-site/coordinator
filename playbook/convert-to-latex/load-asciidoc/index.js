// based on https://gitlab.com/antora/antora/-/tree/main/packages/asciidoc-loader?ref_type=heads
// but with a custom converter (for references)
//
// This only parses files with paths starting with modules/modulename
// This is required to get the module for relative imports.

const Asciidoctor = require('@asciidoctor/core')()
const Converter = require('../convert/converter');


// context is as supplied to the extension (mainly used for logging and imports)
// file is a dictionary with keys {component, version, path, contents, asciidoc_config, content_level} // contents is a Buffer
// catalog is a dictionary which maps from version@component:path to the above dictionary
// returns the parsed asciidoc Document
function loadAsciidoc(context, file, catalog) {
  // register extensions
  const resolve_include_file = require('./resolve-include-file')
  let extensionRegistry = createExtensionRegistry(Asciidoctor, {
    onInclude: (doc, target, cursor) => resolve_include_file(context, target, cursor, file, catalog)
  });

  require('../../extensions/admonitions').register(extensionRegistry, context);
  extensionRegistry.inlineMacro(function() {
    let emojiMap = require('../../extensions/emoji/emoji-map');
    const self = this;
    self.named('emoji');
    self.process(function (parent, target, attrs){
      if (target in emojiMap) {
        return self.createInline(parent, 'directlatex', `\\emoji{${emojiMap[target]}}`);
      }else{
        console.warn(`Skipping emoji inline macro. ${target} not found`)
        return self.createInline(parent, 'quoted', `[emoji ${target} not found]`);
      }
    })
  })


  const opts = {
    extension_registry: extensionRegistry,
    safe: 'safe',
    converter: new Converter(file, file.content_level),
    attributes: Object.assign(
      {'page-partial': '@'}, // i am unsure what this does
      file.asciidoc_config.attributes ?? {},
    ),
  };

  return Asciidoctor.load(file.contents, opts);
}

module.exports = loadAsciidoc;


// files from https://gitlab.com/antora/antora/-/blob/main/packages/asciidoc-loader which are not exported

// From https://gitlab.com/antora/antora/-/blob/main/packages/asciidoc-loader/lib/create-extension-registry.js?ref_type=heads
function createExtensionRegistry (Asciidoctor, callbacks) {
  const IncludeProcessor = require('./include-processor');

  const registry = Asciidoctor.Extensions.create()
  registry.includeProcessor(IncludeProcessor.$new(callbacks.onInclude))
  return registry
}

