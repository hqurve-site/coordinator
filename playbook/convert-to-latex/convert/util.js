// @ts-check

// both strings are encoded text
/** @type function(string, number): string */
module.exports.createHeadingAtLevel = function(text, level) {
  if (level === 0){
    throw "There is no level 0 titles. Please ensure that this never happens";
  }

  let macro = {
    1: "chapter",
    2: "section",
    3: "subsection",
    4: "subsubsection",
    5: "paragraph",
    6: "subparagraph",
  }[level];

  if (!macro) {
    throw `Unknown section level ${level}: text: ${text}`;
  }
  return module.exports.encodeLatex(`\\${macro}{`) + text + encodeLatex("}");
}

/** @type function(string, boolean): string */
module.exports.indentText = function(text, prefix) {
  if (prefix) {
    return text.replaceAll(/^/mg, "    ") // replace each starting line (including first). m=lines, g=global
  }else{
    return text.replaceAll("\n", "\n    ") // replace new lines
  }
}

// makes labels valid latex
/** @type function(string): string */
module.exports.sanitizeLabel = function(string) {
  return string
    .replaceAll("_", "-")
}

/** @type function(number, number): Array<number> */
module.exports.range = function(start, end) {
  return Array.from({length: end - start}, (_v, i) => i + start)
}

// Escaping
// We want to do the following process
// 1. escape any manually written latex or latex in stem blocks (for later decoding)
// 2. escape any characters which are normal in asciidoc but not normal in latex (eg {)
// 3. decode latex

// mimic html unicode, but with latex- prefix
// Perhaps a better solution is to simply encode every character. But I believe it is only a few characters we care about for now
// list of original, encode, final
// Start with a space to avoid issues with urls
/** @type Array<[string, string, string]> */
const LATEX_REPLACEMENTS = [
  // ['&', '&latex-ampersand;', '&'],
  // ['\\\\' ' &latex-doublebackslash;', '\\\\'],
  ['&lt;', ' &latex-lessthan;', '<'],
  ['&gt;', ' &latex-greaterthan;', '>'],
  ['&#43;', ' &latex-plus;', '+'],
  ['&amp;', ' &latex-ampersand;', '&'],
  ['%', ' &latex-percentage;', '%'],
  ['\\', ' &latex-backslash;', '\\'],
  ['{', ' &latex-openbrace;', '{'],
  ['}', ' &latex-closebrace;', '}'],
  ['_', ' &latex-underscore;', '_'],
  ['$', ' &latex-dollarsign;', '$'],
  ['^', ' &latex-caret;', '^'],
  ['#' , ' &latex-hash;', '#'],
  ['`', ' &latex-backtick;', '`'],
  ['\'', ' &latex-quote;', '\''],
  ['"', ' &latex-doublequote;', '"'],
];

const NORMAL_TEXT_REPLACEMENTS = {
  "\\" : "&temptextbacklash;", // replaced at the end. Cannot do before due to replacements of { and }
  "{" : "\\{",
  "}" : "\\}",
  '%': '\\%',
  "&#8201;" : " ",
  "&#8203;" : "",
  "&#8212;" : "--",
  "&#8217;" : "'",
  "&#8230;" : "...",
  "&#8482;" : "{\\texttrademark}",
  "&amp;" : "\\&",
  "&checkmark;" : "{\\checkmark}", // from amssymb
  "&cross;": "{\\xmark}",
  "&lt;" : "{\\textless}",
  "&gt;" : "{\\textgreater}",
  "_": "\\_",
  "#": "\\#",
  "&temptextbacklash;": "{\\textbackslash}"
}

// Escape latex
/** @type function(string): string */
function encodeLatex(string) {
  // specially encode &amp
  string = string.replaceAll("&amp;", "&");
  for (let [s, r, f] of LATEX_REPLACEMENTS) {
    string = string.replaceAll(s, () => r);
  }
  return string
}
module.exports.encodeLatex = encodeLatex;
module.exports.encodeLatexListing = encodeLatex; // for now, they are the same


// after we finish, we need to clean up substitutions.
// Currently, we cannot take off substitutions https://github.com/asciidoctor/asciidoctor/issues/1061
// Cleanup based on https://github.com/asciidoctor/asciidoctor/blob/354a22ffdd753f4760cbfca4db96a4b77937b1ad/lib/asciidoctor/converter/manpage.rb#L705
// Also see https://docs.asciidoctor.org/asciidoc/latest/subs/replacements/
/** @type function(string): string */
module.exports.postProcess = function(string) {
  for (let [s, r] of Object.entries(NORMAL_TEXT_REPLACEMENTS)) {
    string = string.replaceAll(s,() => r);
  }

  // decode latex
  for (let [s,r, f] of LATEX_REPLACEMENTS) {
    string = string.replaceAll(r,() => f); // note we do it in reverse
  }

  return string;
}
