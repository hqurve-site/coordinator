// based on https://docs.asciidoctor.org/asciidoctor.js/latest/extend/converter/custom-converter/#custom-converter-class
//
// This is to be used once per document.
//
// spec: {version, component, module, filename} // used for reference information guessing
// content_level: int

const assert = require('node:assert').strict; // https://nodejs.org/api/assert.html
const {createHeadingAtLevel, indentText, sanitizeLabel, range, encodeLatex, encodeLatexListing} = require('./util');

const parseResourceId = require('@antora/content-classifier/util/parse-resource-id');


// a class to convert
class Converter {
  constructor(spec, content_level) {
    this.spec = spec;
    this.content_level = content_level;

    this['$convert'] = (node, transform) => this.convert(node, transform);
  }

  _make_label(id) {
    if (!id) return "";

    let {version, component, module, filename} = this.spec;

    let label = `${version}--${component}:${module}:page__${filename}__${id}`;

    return encodeLatex("\\label{") + sanitizeLabel(label) + encodeLatex("}");

  }

  // TODO: use actual default version
  _make_reference({version="develop", component, module, family="page", filename, fragment=undefined, text=undefined}) {
    let target = `${version}--${component}:${module}:${family}__${filename}`;
    if (fragment) {
      target += "__" + fragment
    }

    // be sure to enclose the text with {} in case it has square brackets
    return encodeLatex("\\myautoref[{") + (text || "") + encodeLatex("}]{") + sanitizeLabel(target) + encodeLatex("}");
  }

  // Sometimes, the node does not have a way to convert text (eg attributions in quotation blocks).
  // This function takes raw asciidoc text and returns encoded text
  // The document is also required to get attributes.
  _convert_raw_text(text, document) {
    if (!text) {
      return "";
    }
    let Asciidoctor = require('@asciidoctor/core')();

    let opts = {
      extension_registry: document.getExtensions(),
      safe: 'safe',
      converter: this,
      attributes: document.getAttributes(),
    };

    let output = Asciidoctor.convert(text, opts);

    assert(typeof(output) === 'string');
    return output;
  }

  convert(node, transform) {
    // In this function, we prefer get methods since they trigger internal conversion functions
    const name = transform || node.getNodeName();

    if (name === "document") {
      // simply handle subs
      return node.getBlocks().map(n => n.convert()).join("\n\n");
    } else if( name === "embedded") {
      // todo: investigate more
      return node.getBlocks().map(n => n.convert()).join("\n\n");
    } else if( name === "open") {
      return node.getBlocks().map(n => n.convert()).join("\n\n");
    } else if( name === "preamble") {
      assert(!node.getTitle());
      // simply handle subs
      return node.getBlocks().map(n => n.convert()).join("\n\n");
    } else if( name=== "paragraph") {
      // todo: investigate more
      // return node.convert(); // simply convert inside // causes infinite recursion
      return [node.getContent(),... node.getBlocks().map(n => n.convert())].join("\n\b");
    } else if( name=== "inline_directlatex") { // a custom type especially for emojis
      return encodeLatex(node.text);
    } else if( name=== "inline_break") {
      return node.getText() + encodeLatex("\\newline");
    }else if (name === "inline_quoted") {
      // console.log(node);
      let roles = node.getRoles(); // get a list of roles (strings)
      let type = node.getType(); // get the type

      let content; // store before processing rules
      if (type === "latexmath") {
        content = encodeLatex(`\\(${node.getText()}\\)`);
      }else if (type === "emphasis") {
        content = encodeLatex(`\\emph{`) + node.getText() + encodeLatex("}");
      }else if (type === "single") {
        content = encodeLatex("`") + node.getText() + encodeLatex("'");
      }else if (type === "double") {
        content = encodeLatex("``") + node.getText() + encodeLatex("''");
      }else if (type === "monospaced") {
        content = encodeLatex("\\texttt{") + node.getText() + encodeLatex("}");
      }else if (type === "unquoted") {
        content = node.getText(); // do nothing
      }else if (type === "strong") {
        content = encodeLatex(`\\textbf{`) + node.getText() + encodeLatex("}");
      }else if (type === "superscript") {
        content = encodeLatex(`\\textsuperscript{`) + node.getText() + encodeLatex("}");
      }else if (type === "subscript") {
        content = encodeLatex(`\\textsubscript{`) + node.getText() + encodeLatex("}");
      }
      
      if (content !== undefined) {
        // now, handle roles
        if (roles.length === 0) {
          return content;
        }else if(roles.length === 1 && roles[0] === "line-through") {
          return encodeLatex("\\strikethrough{") + content + encodeLatex("}");
        }
      }
    }else if(name === "inline_anchor") {
      if (node.getType() === "xref") {

        let resource_id = (node.getAttributes().path || "").replace(/\.html$/, ".adoc"); // replace trailing extension
        let fragment = node.getAttributes().fragment || undefined;

        let result;
        if (resource_id) {
          result = parseResourceId(resource_id, {
            version: this.spec.version,
            component: this.spec.component,
            module: this.spec.module,
            family: "page",
            relative: this.spec.filename,
          })
        }else{ // if the xref is just #fragment
          result = {
            version: this.spec.version,
            component: this.spec.component,
            module: this.spec.module,
            family: "page",
            relative: this.spec.filename,
          };
        }

        if (result) {
          return this._make_reference({
            version: result.version,
            component: result.component,
            module: result.module,
            family: result.family,
            filename: result.relative,
            text: node.getText() || undefined,
            fragment: fragment,
          })
        }else{
          return `unmatchedxref:${node.target}[${node.getText()}]`;
        }
      } //end xref
      if (node.getType() === "link") {
        let text= node.getText();
        let target = node.target;

        if (text === node.target) {
          text = undefined;
        }
        if (text) {
          return encodeLatex("\\href{") + target + encodeLatex("}{") + text + encodeLatex("}");
        }else{
          return encodeLatex("\\url{") + target + encodeLatex("}");
        }
      }
    }else if(name === "section") {
      let id = node.getId();
      let text = node.getTitle();
      let level = node.level;

      let section_text = `${createHeadingAtLevel(text, level + this.content_level)}\n${this._make_label(id)}`;

      return [section_text, ... node.getBlocks().map(n => n.convert())].join("\n\n");
    }else if(name === "floating_title") {
      if (node.style == "discrete") {
        let text = node.getTitle();

        let section_text = encodeLatex(`\\discretefloatingtitle{${text}}`);

        return [section_text, ... node.getBlocks().map(n => n.convert())].join("\n\n");
      }
    }else if(name === "ulist") {
      return (
        encodeLatex("\\begin{itemize}\n")
        + node.getBlocks().map(n => indentText(n.convert())+ "\n").join("")
        + encodeLatex("\\end{itemize}")
      );
    }else if(name === "dlist") {
      // https://asciidoctor.github.io/asciidoctor.js/main/#listitem
      // blocks in a dlist are a tuple of [[terms], description].
      // Each of the nodes are a list_item. So, we handle everything here
      // After testing, it appears that items without any content are grouped with the next
      let blocks = node.getBlocks().flatMap(([terms, desc]) => {
        assert(terms.length >= 1);
        return [...terms.slice(0, -1).map(t => [t, undefined]), [terms[terms.length -1], desc]]
      });
      return (
        encodeLatex("\\begin{description}\n")
        + blocks.map(([term, desc]) => {
          let title = term.getText();

          let content;
          if (desc !== undefined && desc.getText && desc.getBlocks) {
            content = [desc.getText(), ... desc.getBlocks().map(n => n.convert())].join("\n\n");
          }

          return indentText(encodeLatex("\\item[") + (title || "") + encodeLatex("] ") + (content ?? "")) + "\n";
        }).join("")
        + encodeLatex("\\end{description}")
      );
    }else if(name === "list_item") {
      return [encodeLatex("\\item ") + node.getText(), ... node.getBlocks().map(n => n.convert())].join("\n\n");
    }else if(name === "olist") {
      let attributes = node.getAttributes();
      let args = {
        "arabic": "label=\\arabic*)",
        "loweralpha": "label=\\alph*)",
        "upperalpha": "label=\\Alph*)",
        "lowerroman": "label=\\roman*)",
        "upperroman": "label=\\Roman*)",
      }[attributes.style];
      if (args !== undefined) {
        if (attributes.start) {
          args += `, start=${attributes.start}`;
        }
        return (
          encodeLatex(`\\begin{enumerate}[${args}]\n`)
          + node.getBlocks().map(n => indentText(n.convert())+ "\n").join("")
          + encodeLatex("\\end{enumerate}")
        );
      }
    }else if(name === "admonition") {
      // get the content
      let content;
      if (node.content_model === "simple") {
        content = [node.getContent()];
      }else if (node.content_model === "compound") {
        content = node.getBlocks().map(n => n.convert());
      }

      let attributes = node.getAttributes();
      if (content){
        let style = attributes.style.toLowerCase();
        return (
          encodeLatex(`\\begin{admonition-${style}}[{`) + (node.getTitle() ?? "") + encodeLatex("}]\n")
          + content.join("\n\n")
          + encodeLatex(`\n\\end{admonition-${style}}`)
        )
      }
    }else if(name === "example") {
      let roles = node.getRoles();
      if (roles.length === 0) {
        return (
          encodeLatex("\\begin{example}[{") + (node.getTitle() ?? "") + encodeLatex("}]\n")
          + node.getBlocks().map(n => n.convert()).join("\n\n")
          + encodeLatex("\n\\end{example}")
        )
      }
      if (roles.length === 1 && roles[0] === "proof") {
        return (
          encodeLatex("\\begin{proof}[{") + (node.getTitle() ?? "") + encodeLatex("}]\n")
          + node.getBlocks().map(n => n.convert()).join("\n\n")
          + encodeLatex("\n\\end{proof}")
        )
      }
    }else if(name === "sidebar") {
      let roles = node.getRoles();
      let environment;

      if (roles.length === 0) {
        environment = "sidebar";
      }else if(roles.length === 1) {
        environment = {
          'theorem': 'theorem',
          'corollary': 'corollary',
          'lemma': 'lemma',
          'proposition': 'proposition'
        }[roles[0]];
      }
      if (environment) {
        return (
          encodeLatex(`\\begin{${environment}}[{`) + (node.getTitle() ?? "") + encodeLatex("}]\n")
          + node.getBlocks().map(n => n.convert()).join("\n\n")
          + encodeLatex(`\n\\end{${environment}}`)
        )
      }
    }else if(name === "literal") {
      return (
        encodeLatex("\\begin{literal}[{") + (node.getTitle() ?? "") + encodeLatex("}])")
        + this._make_label(node.getId()) + encodeLatex("\n")
        + encodeLatexListing(node.lines.join("\n"))
        + encodeLatex("\n\\end{literal}")
      )
    }else if(name === "listing") {
      return (
        encodeLatex("\\begin{listing}[{") + (node.getTitle() ?? "") + encodeLatex("}]\n")
        + encodeLatexListing(node.lines.join("\n"))
        + encodeLatex("\n\\end{listing}")
      )
    }else if(name === "stem") {
      let attributes = node.getAttributes();
      if (attributes.style === "latexmath") {
        return encodeLatex(
          "\\begin{equation*}\n"
          + node.lines.map(l => l + "\n").join("")
          + "\\end{equation*}"
        );
      }
    }else if(name === "image" || name === "inline_image") {
      let attributes = node.getAttributes();
      let title, target;
      if (name === "image") {
        title = node.getTitle();
        target = attributes.target;
      }else {
        title = node.getText();
        target = node.target;
      }
      let label = this._make_label(node.getId());
      let params = [];
      if (attributes.width) {
        let width = `${attributes.width}`;
        if (width.endsWith('%')) {
          width = +width.slice(0,-1);
          params.push(`width=${width / 100}\\linewidth`);
        }
      }

      let raw_image;// \includegraphics[params]....
      if (target.startsWith("https://")) {
        // set draft with width and height
        raw_image = encodeLatex(`\\includegraphics[draft,width=8cm,height=5cm]{placeholder.png}\n`);
        title = (title ||"") + encodeLatex(` (omitted external image: ${target})`);
      }else {
        // strip known extensions if not external. The includegraphics command is capable of automatically selecting extensions
        target = target.replace(/\.(png|jpg)$/, "");
        let result = parseResourceId(target, {
          version: this.spec.version,
          component: this.spec.component,
          module: this.spec.module,
          family: "page",
          relative: this.spec.filename,
        }, "images");
        raw_image = encodeLatex(`\\includegraphics[${params.join(',')}]{images/${result.version}-${result.component}/${result.module}/${result.relative}}\n`)
      }

      if (attributes.subfigure_width) { // custom attribute
        let width = `${attributes.subfigure_width}`;
        assert(width.endsWith('%'));
        return (
          encodeLatex(`\\begin{subfigure}{${+width.slice(0,-1) / 100}\\textwidth}\\centering\n`)
          + raw_image
          + (title ? encodeLatex("\\caption{") + title + encodeLatex("}\n") + (label ? label + encodeLatex('\n'): ""): "")
          + encodeLatex("\\end{subfigure}")
        );
      }else{
        return (
          encodeLatex("\\begin{figure}[H]\\centering\n")
          + raw_image
          + (title ? encodeLatex("\\caption{") + title + encodeLatex("}\n") + (label ? label + encodeLatex('\n'): ""): "")
          + encodeLatex("\\end{figure}")
        );
      }
    }else if(name === "video") {
      let attributes = node.getAttributes();
      if (attributes.poster === "youtube") {
        return encodeLatex(`[Youtube video \\href{https://youtube.com/watch?v=${attributes.target}}]`);
      }
    }else if(name === "quote") {
      let attributes = node.getAttributes();
      let attribution = this._convert_raw_text(attributes.attribution || "", node.getDocument());
      let cite_title = this._convert_raw_text(attributes.citetitle || "", node.getDocument());

      return (
        encodeLatex("\\begin{custom-quotation}[{") + attribution + encodeLatex("}][{") + cite_title + encodeLatex("}]")
        + node.getBlocks().map(n => "\n" + n.convert() + "\n").join("")
        + encodeLatex("\\end{custom-quotation}")
      );
    }else if(name === "verse") {
      // according to https://docs.asciidoctor.org/asciidoc/latest/blocks/verses/, it is basically <pre>
      // which preserves formatting while not being verbatim
      // We use the alltt package https://ctan.org/pkg/alltt
      // However, I dont think this is quite right. Instead I'll just use a verbatim instead
      // TODO: handle attributions
      return (encodeLatex("\\begin{verbatim}\n")
        + node.getContent()
        + encodeLatex("\n\\end{verbatim}")
      );
    }else if(name === "table") {
      // parse attributes
      let has_header = false;
      let grid = "all"; // one of all, rows, cols, or none
      let frame = "all"; // one of all, ends, sides or none
      let auto_scale = false; // scales the table automatically to fit to width
      let mode = undefined; // either "figure", "nested", "multicol"
      for (let attribute of Object.entries(node.getAttributes())) {
        let [k,v] = attribute;

        if (k === "tablepcwidth" && v === 100) continue; // table percent width
        if (k === "colcount" || k === "rowcount" || k === "cols" || k === "style") continue;

        if (k === "header-option") {
          has_header = true;
          continue;
        }
        if (k === "frame") {
          frame = v;
          continue;
        }
        if (k === "grid") {
          grid = v;
          continue;
        }
        if (k === "autoscale" && v == "true") {
          auto_scale = true;
          continue;
        }
        if (k === "mode" && (v === "figure" || v === "nested" || v === "multicol")) {
          mode = v;
          continue;
        }
        if (k === "attribute_entries") continue; // I have no idea what this is for
        if (k === "$positional" && v.every(i => i == undefined)) continue; // I have no idea what this is for
        if (k === "autowidth-option") continue; // do nothing TODO
        if (k === "separator") continue; // do nothing since this is a parser attribute
        
        console.log(attribute);
        console.log(node.getAttributes())
        assert.fail(`Unknown table attribute ${attribute}`)
      }
      // end parse table attributes

      // see https://ctan.joethei.xyz/macros/latex/contrib/tabularray/tabularray.pdf

      // multirows and multicolumns cause there to be missing cells. We recreate the layout
      // sections = Array of ["head"|"body|"foot", 2d array which an array of rows each of which contains cells]
      // In the 2d array
      //  - undefined means that there is no cell occupying it
      //  - null if there is a cell occupying it (through span)
      //  - normal cell if the top left hand corner of the cell starts here
      let sections = node.rows.bySection().map(([sec, rows]) => {
        let corrected_rows = Array.from({length: rows.length}, () => Array.from({length: node.columns.length}, () => undefined));

        for (let [row_index, row] of rows.entries()) {
          let col_index = 0;

          for (let cell of row) {
            // skip occupied
            while (corrected_rows[row_index][col_index] === null) {
              col_index++;
            }
            assert(col_index < node.columns.length);

            let row_span = cell.getRowSpan() ?? 1;
            let column_span = cell.getColumnSpan() ?? 1;

            // ensure unoccupied
            for (let i of range(row_index, row_index + row_span)) {// for each row index
              for (let j of range(col_index, col_index + column_span)) {// for each column
                assert(corrected_rows[i][j] === undefined);
                corrected_rows[i][j] = null; // occupy
              }
            }
            // insert cell
            corrected_rows[row_index][col_index] = cell

            // go to next column
            col_index ++;
          }
        }

        return [sec, corrected_rows];
      }); // end normalize rows by section

      // now, map each cell
      // this is a single string which ends with a newline
      let converted_cells = sections.filter(([_sec, rows]) => rows.length > 0).map(([sec, rows]) => {
        // map each row to a string which ends with a newline 
        let converted_rows = rows.flatMap(row => row.map(cell => {
          if (cell === undefined || cell === null) {
            return "";
          }
          let attrs = cell.getAttributes();

          let row_span = cell.getRowSpan() ?? 1;
          let column_span = cell.getColumnSpan() ?? 1;

          let content = [
              this._convert_raw_text(cell.text, cell.document), // use instead of getText since getText is inconsistent (based on cell content type / column type)
              ...cell.getBlocks().map(n => n.convert())
            ].join("\n\n").replaceAll(/\n\s*\n/gm, encodeLatex("\\\\")) // replace multiple skiplines (not sure why)
          ;

          // we do not want to spam tables with \\SetCell. So, we only use it when necessary

          let span_info = undefined;
          let align_info = undefined;

          if (row_span !== 1 || column_span !== 1) { // check if span is different from usual
            span_info = `[r=${row_span}, c=${column_span}]`;
          }
          // check if align is different from usual
          let col_attrs = node.columns[attrs.colnumber-1].getAttributes();
          if (col_attrs.halign !== attrs.halign || col_attrs.valign !== attrs.valign) {
            let valign = { top: "h", middle: "m", bottom: "f" }[attrs.valign];
            let halign = { left: "l", center: "c", right: "r" }[attrs.halign];
            align_info = `{${valign}, ${halign}}`;
          }

          // combine both
          let extra_cell_info;
          if (span_info) {
            extra_cell_info = span_info + (align_info ?? "{}");
          }else if(align_info) {
            extra_cell_info = align_info;
          }

          if (extra_cell_info) { 
            return encodeLatex(`\\SetCell${extra_cell_info}{`) + content + encodeLatex("}");
          }else{
            return encodeLatex("{") + content + encodeLatex("}");
          }
        }).join(encodeLatex(" &amp; "))).map(r => r + encodeLatex(" \\\\\n"));

        if (sec === "head") {
          converted_rows = converted_rows.map(r => encodeLatex("\\SetRow{font=\\bfseries}") + r);
        }

        // join into a single string which ends on new line
        return converted_rows.join(grid === "all" || grid === "rows" ? encodeLatex("\\hline\n") : "")
      }).join(grid === "all" || grid === "rows" ? encodeLatex("\\hline[\\thicktableline]\n") : "");

      // construct column options
      let column_options = node.columns.map(column => {
        let attributes = column.getAttributes();
        let valign = attributes.valign; // top middle bottom
        let halign = attributes.halign; // left center right
        let colpcwidth = attributes.colpcwidth;

        valign = { top: "h", middle: "m", bottom: "f" }[valign];
        halign = { left: "l", center: "c", right: "r" }[halign];
        return encodeLatex(`${mode === "figure" ? "X" : "Q"}[${halign},${valign}]`);
      });
      let column_spec = column_options.join(grid === "all" || grid === "cols" ? "|" : " ");
      if (frame === "all" || frame === "sides") {
        column_spec = "|[\\thicktableline]" + column_spec + "|[\\thicktableline]";
      }

      let table_text = (
        encodeLatex(`\\begin{tblr}{colspec={${column_spec}}, measure=vbox}\n`)
         + (frame === "all" || frame === "ends" ? encodeLatex("\\hline[\\thicktableline]\n") : "")
         + converted_cells
         + (frame === "all" || frame === "ends" ? encodeLatex("\\hline[\\thicktableline]\n") : "")
         + encodeLatex("\\end{tblr}")
      );
      if (auto_scale) {
        table_text = (
          encodeLatex(`\\begin{adjustbox}{width=\\linewidth}\n`)
          + table_text
          + encodeLatex(`\n\\end{adjustbox}`)
        );
      }

      let title = node.getTitle();

      // TODO: custom table attributes
      if (mode === "figure") {
        return (
          encodeLatex("\\begin{figure}[H]\\centering\n")
          + table_text +"\n"
          + (title ? encodeLatex("\\caption{") + title + encodeLatex("}\n") + this._make_label(node.getId()) + encodeLatex("\n"): "")
          + encodeLatex("\\end{figure}")
        );
      }else if (mode === "multicol") {
        // find body
        let body_section = sections.find(([sec, _rows]) => sec === "body");
        // check that there is exactly one row
        if (body_section != null && body_section[1].length === 1) {
          let columns = body_section[1][0];
          let columns_text = columns.map(cell => {
            if (cell === undefined || cell === null) {
              return "";
            }
            return [
                this._convert_raw_text(cell.text, cell.document), // use instead of getText since getText is inconsistent (based on cell content type / column type)
                ...cell.getBlocks().map(n => n.convert())
            ].join("\n\n");
          });
          return (
            encodeLatex(`\\begin{multicols}{${columns.length}}\n`)
            + columns_text.join(encodeLatex("\n\\columnbreak\n"))
            + encodeLatex(`\\end{multicols}`)
          )
        }else {
          console.log("Unexpected sections for multicol");
          console.log(sections)
        }
      }else if(mode === "nested") {
        // TODO: add caption for nested table
        return table_text;
      }else{
        return (
          encodeLatex("\\begin{table}[H]\\centering\n")
          + (title ? encodeLatex("\\caption{") + title + encodeLatex("}\n") + this._make_label(node.getId()) + encodeLatex("\n"): "")
          + table_text +"\n"
          + encodeLatex("\\end{table}")
        );
      }
    }
    // fallout. Each of the above should exit the function

    // cleanup before printing
    delete node.document;
    delete node.parent;
    delete node.reader;
    delete node.converter;
    // this.logger.warn(`Unknown node type: ${name}`)
    console.log(node);
    assert.fail(`Unknown node type: "${name}"`);
  }
}
module.exports = Converter;
