// Types for antora/asciidoc

/** @typedef {(context: AntoraContext) => void } AntoraExtensionRegister */
/** @typedef {object} AntoraContext
  * @property {any} getLogger
  * @property {function("contentAggregated", function({siteCatalog: AntoraSiteCatalog, contentAggregate: AntoraContentAggregate}))} on
  */


/** @typedef {object} AntoraSiteCatalog
  * @property {function({contents: Buffer, out: {path: string}})} addFile
  */

/** @typedef {Array<AntoraContentAggregateEntry>} AntoraContentAggregate */
/** @typedef {object} AntoraContentAggregateEntry 
  * @property {string} name
  * @property {string} title
  * @property {string} version
  * @property {Array<AntoraFile>} files
  * @property {object} [book] - This is a custom addition
  * @property {string} book.title
  * @property {bool} [book.singleChapter]
  * @property {Array<AntoraContentAggregateBookEntry>} [book.layout]
  */

/** @typedef {object} AntoraContentAggregateBookEntry
  * @property {string} src - Of the form <module>:filepath
  * @property {string} [title]
  * @property {number} [level]
  * @property {number | "omit"} [titleLevel]
  * @property {number} [contentLevel]
  */

/** @typedef {object} AntoraFile
  * @property {{path: string, origin:{descriptor: {name: string, version: string, asciidoc: {attributes: object}}}}} src
  * @property {Buffer} _contents
  */

// /** @typedef {object} AsciidocDocument
//   */
