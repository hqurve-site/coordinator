# Layout

- [./fonts](./fonts) contains fonts for use.
- [./playbook](./playbook/) contains the antora instructions to build the site.
- [./tools](./tools)
    - `combine-content-creation-info` contains scripts used to combine the individual content directories into one.
        Kept for accounting purposes

    - `combine-notes-creation-info` contains scripts used to combine the individual notes directories into one.
        Kept for accounting purposes

    - `compress-images` looks for folders called `_raw-images` (recursively) and compresses images (reduces dimensions)
    according to the `config.json` file.
    The original files are left unchanged. This is done for large images (eg photographs).

        Example config file
        ```json
        {
            "file1.jpg": { "dimensions": 2000, "size": 1000 },
            // ...
        }
        ```
        All unmatched files are assumed to have `{"dimensions": 600, "size": 50}`.
        The following commands are done for each file
        ```bash
        convert $filename -resize "$($dimensions)x" $outfile # uses imagemagick
        jpegoptim --size=$size $outfile # uses https://github.com/tjko/jpegoptim
        ```
        Only jpg files are supported. The outfile has the same name as the original file
        but is in the same directory as `_raw-images`.


    - `gen-images` looks for all svg files and converts them to png using inkscape.
        This is done to ensure that svg images are displayed correctly.

    - `local-to-antora` contains the code used to convert latex documents to notes in antora. This should not need to be used anymore.

    - `parallel-latexmk.sh`
