// @ts-check

main();
function main() {
  let args = process.argv;
  args = args.slice(2) // skip node and filename

  if (args[0] === "live-server") live_server()
  else console.log("unrecognized")
}

function live_server() {
  const chokidar = require('chokidar');

  // watch for content changes and rebuild
  const input_watcher = chokidar.watch(
    ['./content', './antora-ui'],
    {
      awaitWriteFinish: true,
      ignored: /(^|\/)\../, // ignore dotfiles
    },
  );
  input_watcher.on('ready', () => {
    let builder = make_buffered_runner(() => run_command('npm', ['run', 'antora-local']));
    builder(); // run at least once
    input_watcher.on('all', (name, path) => { 
      console.log(name, path);
      builder();
    })
  });

  run_command('npm', ['run', 'vite-server'])
}

/** @param {string} cmd
  * @param {Array<string>} args
  * @param {object} options
  * @returns {Promise<void>}
  */
function run_command(cmd, args, options = {}) {
  let child_process = require('child_process');
  options.stdio = 'inherit';
  
  return new Promise((resolve, reject) => {
    let handle = child_process.spawn(cmd, args, options);

    handle.on('error', reject);
    handle.on('close', () => resolve());
  })
}


/** @param {function(): Promise<void>} f - function to be called
  * @param {number} delay_ms
  * @param {number} wait_ms
  * @returns {function(): void} - buffered version of the same function
  */
function make_buffered_runner(f, delay_ms=50, wait_ms=100) {
  let counter = 0;
  let is_running = false;

  // exec immediately if not running, otherwise wait
  /** @type {function(number): void} */
  let waiter = my_counter => {
    if (my_counter === counter) {
      if (is_running) {
        // wait
        setTimeout(() => waiter(my_counter), wait_ms)
      }else{
        // run
        is_running = true;
        f().then(() => {
          is_running = false;
        })
      }
    }
  };

  return () => {
    counter += 1;
    let my_counter = counter;
    setTimeout(() => waiter(my_counter), delay_ms)
  }
}
